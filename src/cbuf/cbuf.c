#include <cbuf/cbuf.h>

#include <stdlib.h>                              //malloc()
#include <wchar.h>                               //wchar_t
#include <errno.h>                               //errno
#include <string.h>                              //strncat()


Cbuf *
cbuf_new(void)
{
	Cbuf *cbuf = malloc(sizeof(Cbuf));
	if(cbuf == NULL)
		return NULL;

	cbuf->capacity = CBUF_GROW_CAPACITY;
	cbuf->data = malloc(cbuf->capacity * sizeof(char));
	if(cbuf->data == NULL)
	{
		free(cbuf);
		return NULL;
	}
	cbuf->data[0] = '\0';

	return cbuf;
} /*cbuf_new()*/


void
cbuf_del(Cbuf *cbuf)
{
	if(cbuf == NULL)
		return;
	if(cbuf->data != NULL)
		free(cbuf->data);
	free(cbuf);
	cbuf = NULL;
} /*cbuf_del()*/



int
cbuf_strcat(Cbuf *cbuf, const char *str)
{
	int rv = cbuf_realloc(cbuf, strlen(cbuf->data) + strlen(str) + 1);
	if(rv != 0)
		return rv;

	strncat(cbuf->data, str, cbuf->capacity - strlen(cbuf->data) - 1);
	cbuf->data[cbuf->capacity -1] = '\0';

	return 0;
} /*cbuf_strcat()*/


int
cbuf_wcscat(Cbuf *cbuf, const wchar_t *wcs)
{
	int rv = -3;
	char tmp_str[W2MB_RATIO + 1] = "";
	int i = 0;

	while(wcs[i] != L'\0')
	{
		snprintf(tmp_str, W2MB_RATIO, "%lc", wcs[i]);
		tmp_str[W2MB_RATIO] = '\0';
		rv = cbuf_strcat(cbuf, tmp_str);
		if(rv != 0)
			return rv;
		i++;
	}

	return 0;
} /*cbuf_wcscat()*/


int
cbuf_wcscpy(Cbuf *cbuf, const wchar_t *wcs)
{
	cbuf->data[0] = '\0';
	return cbuf_wcscat(cbuf, wcs);
} /*cbuf_wcscpy()*/


char *
cbuf_getstr(Cbuf *cbuf)
{
	return cbuf->data;
} /*cbuf_getstr()*/


size_t
cbuf_strlen(Cbuf *cbuf)
{
	return strlen(cbuf->data);
} /*cbuf_strlen()*/


static int
cbuf_realloc(Cbuf *cbuf, const size_t target_size)
{
	while(cbuf->capacity < target_size)
	{
		cbuf->capacity += CBUF_GROW_CAPACITY;
		if(cbuf->capacity > CBUF_MAX_CAPACITY)
		{
			errno = ENOMEM; //???
			return -1;
		}

		cbuf->data = realloc(cbuf->data,
				cbuf->capacity * sizeof(char));
		if(cbuf->data == NULL)
			return -2;
	}

	return 0;
} /*cbuf_realloc()*/
