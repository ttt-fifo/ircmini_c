#ifndef CBUF_H
#define CBUF_H


#include <stdlib.h>                              //size_t
#include <wchar.h>                               //wchar_t


#define CBUF_GROW_CAPACITY 4096
#define CBUF_MAX_CAPACITY 65536
#define W2MB_RATIO 4


typedef struct Cbuf
{
	char *data;
	size_t capacity;
} Cbuf;


Cbuf *
cbuf_new(void);

void
cbuf_del(Cbuf *cbuf);

int
cbuf_strcat(Cbuf *cbuf, const char *str);

int
cbuf_wcscat(Cbuf *cbuf, const wchar_t *wcs);

int
cbuf_wcscpy(Cbuf *cbuf, const wchar_t *wcs);

char *
cbuf_getstr(Cbuf *cbuf);

size_t
cbuf_strlen(Cbuf *cbuf);

static int
cbuf_realloc(Cbuf *cbuf, const size_t target_size);


#endif /*CBUF_H*/
