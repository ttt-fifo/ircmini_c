#include <ircmini/ircmini.h>
#include <cbuf/cbuf.h>                           //Cbuf

#include <stdlib.h>                              //malloc()
#include <wchar.h>                               //wchar_t
#include <sys/types.h>                           //complementary to sys/socket
#include <sys/socket.h>                          //socket(), shutdown(), etc.
#include <netdb.h>                               //getaddrinfo()
#include <string.h>                              //memset()
#include <unistd.h>                              //close()
#include <sys/select.h>                          //struct timeval
#include <errno.h>                               //errno


Irc *
irc_new(const char *server, const char *port)
{
	Irc *irc = malloc(sizeof(Irc));
	if (irc == NULL)
		return NULL;

	struct addrinfo hints;                   //hints to getaddrinfo()
        struct addrinfo *servinfo;               //server information

        memset(&hints, 0, sizeof(hints));        //empty the struct
        hints.ai_family = AF_UNSPEC;             //don't care IPv4 or IPv6
        hints.ai_socktype = SOCK_STREAM;         //TCP stream sockets
        hints.ai_flags = AI_PASSIVE;             //fill in my IP for me
	if(getaddrinfo(server, port, &hints, &servinfo) != 0)
	{
		/*need to set errno manually, getaddrinfo() does not set it*/
		errno = EDESTADDRREQ;            //Destination address required

		free(irc);
		irc = NULL;
		return NULL;
	}

	/*create socket*/
        irc->sock = socket(servinfo->ai_family,
	       	           servinfo->ai_socktype,
			   servinfo->ai_protocol);
	if(irc->sock == -1)
	{
		freeaddrinfo(servinfo);
		free(irc);
		irc = NULL;
		return NULL;
	}

	/*connect to server*/
        int rv = connect(irc->sock,
			 servinfo->ai_addr, servinfo->ai_addrlen);
	if(rv == -1)
	{
		freeaddrinfo(servinfo);
		irc_del(irc);
		return NULL;
	}

	freeaddrinfo(servinfo);

	irc->sendbuf = cbuf_new();
	if(irc->sendbuf == NULL)
	{
		irc_del(irc);
		return NULL;
	}

	return irc;
} /*irc_new()*/

void
irc_del(Irc *irc)
{
	if (irc == NULL)
		return;

	/*this design pattern is for sleeping 0.2 sec*/
	struct timeval tv;                       //needed for define sleep time
	tv.tv_sec = 0;                           //zero seconds
	tv.tv_usec = 200000;                     //0.2 seconds

	/*close socket*/
	shutdown(irc->sock, SHUT_RDWR);
	select(0, NULL, NULL, NULL, &tv);        //select sleeps 0.2 seconds
	close(irc->sock);

	if(irc->sendbuf != NULL)
		cbuf_del(irc->sendbuf);

	free(irc);
	irc = NULL;
} /*irc_del()*/

void
irc_catch_ping(Irc *irc, wchar_t *response)
{
} /*irc_catch_ping()*/

int
irc_privmsg(Irc *irc, const wchar_t *recipient,
		const wchar_t *message)
{
	int rv = cbuf_wcscpy(irc->sendbuf, L"PRIVMSG ");
	if(rv != 0) return rv;
	rv = cbuf_wcscat(irc->sendbuf, recipient);
	if(rv != 0) return rv;
	rv = cbuf_strcat(irc->sendbuf, " :");
	if(rv != 0) return rv;
	rv = cbuf_wcscat(irc->sendbuf, message);
	if(rv != 0) return rv;
	rv = cbuf_strcat(irc->sendbuf, "\r\n");
	if(rv != 0) return rv;

	return irc_sendall(irc);
} /*irc_privmsg()*/

void
irc_get_response(Irc irc)
{
} /*irc_get_response()*/


int
irc_raw(Irc *irc, const wchar_t *raw_message)
{
	int rv = cbuf_wcscpy(irc->sendbuf, raw_message);
	if(rv != 0) return rv;
	rv = cbuf_strcat(irc->sendbuf, "\r\n");
	if(rv != 0) return rv;

	return irc_sendall(irc);
} /*irc_raw()*/


static int
irc_sendall(Irc *irc)
{
	char *p_sendbuf;
	p_sendbuf = cbuf_getstr(irc->sendbuf);
	size_t strlen_sendbuf = strlen(p_sendbuf);
	ssize_t sentcount = 0;
	ssize_t sentcount_sum = 0;

	while(sentcount_sum < strlen_sendbuf)
	{
		p_sendbuf += sentcount;
		sentcount = send(irc->sock, p_sendbuf, strlen(p_sendbuf), 0);
		if(sentcount <= 0)
		{
			//errno???
			return -1;
		}
		sentcount_sum += sentcount;
	}

	return 0;
} /*irc_sendall()*/
