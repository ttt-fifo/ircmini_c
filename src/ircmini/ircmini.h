#ifndef IRCMINI_H
#define IRCMINI_H

#include <wchar.h>                               //wchar_t
#include <cbuf/cbuf.h>


typedef struct Irc
{
	int sock;
	Cbuf *sendbuf;
} Irc;


Irc *
irc_new(const char *server, const char *port);

void
irc_del(Irc *irc);

void
irc_catch_ping(Irc *irc, wchar_t *response);

int
irc_privmsg(Irc *irc, const wchar_t *recipient,
            const wchar_t *message);

void
irc_get_response(Irc irc);

int
irc_raw(Irc *irc, const wchar_t *raw_message);

static int
irc_sendall(Irc *irc);


#endif /*IRCMINI_H*/
