#include <cbuf/cbuf.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

/* Create all test functions below ------------------------------------------*/

void
test00(void)
{
	Cbuf *cbuf = cbuf_new();
	if(cbuf == NULL)
	{
		perror("ERROR");
		return;
	}

	printf("cbuf->capacity: %i\n", cbuf->capacity);
	printf("cbuf->data: %s\n", cbuf->data);
	printf("strlen(cbuf->data): %i\n", strlen(cbuf->data));

	cbuf_del(cbuf);
	printf("OK: created/deleted\n");
} /*test00()*/

void
test01(void)
{
	Cbuf *cbuf = cbuf_new();
	if(cbuf == NULL)
	{
		perror("ERROR");
		return;
	}

	int rv = cbuf_wcscpy(cbuf, L"unicode Яѝуь data");
	if(rv != 0)
	{
		perror("ERROR");
		return;
	}

	printf("cbuf->capacity: %i\n", cbuf->capacity);
	printf("cbuf->data: %s\n", cbuf->data);
	printf("strlen(cbuf->data): %i\n", strlen(cbuf->data));

	cbuf_del(cbuf);
	printf("OK: cbuf_wcscpy()\n");

} /*test01()*/

void
test02(void)
{
	Cbuf *cbuf = cbuf_new();
	if(cbuf == NULL)
	{
		perror("ERROR");
		return;
	}

	int rv = cbuf_wcscpy(cbuf, L"unicode Яѝуь data");
	if(rv != 0)
	{
		perror("ERROR");
		return;
	}

	rv = cbuf_strcat(cbuf, " + concatenated");
	if(rv != 0)
	{
		perror("ERROR");
		return;
	}

	printf("cbuf->capacity: %i\n", cbuf->capacity);
	printf("cbuf->data: %s\n", cbuf->data);
	printf("strlen(cbuf->data): %i\n", strlen(cbuf->data));

	cbuf_del(cbuf);
	printf("OK: cbuf_strcat()\n");
} /*test02()*/

void
test03(void)
{
	Cbuf *cbuf = cbuf_new();
	if(cbuf == NULL)
	{
		perror("ERROR");
		return;
	}

	int rv = cbuf_wcscpy(cbuf, L"unicode Яѝуь data");
	if(rv != 0)
	{
		perror("ERROR");
		return;
	}

	printf("cbuf->capacity: %i\n", cbuf->capacity);
	printf("cbuf_getstr(): %s\n", cbuf_getstr(cbuf));
	printf("cbuf_strlen(): %i\n", cbuf_strlen(cbuf));

	cbuf_del(cbuf);
	printf("OK: cbuf_getstr() and cbuf_strlen()\n");
} /*test03()*/

void
test04(void)
{
	Cbuf *cbuf = cbuf_new();
	if(cbuf == NULL)
	{
		perror("ERROR");
		return;
	}

	int rv = cbuf_wcscpy(cbuf, L"unicode Яѝуь data");
	if(rv != 0)
	{
		perror("ERROR");
		return;
	}

	rv = cbuf_wcscat(cbuf, L" + more unicode Ихбъ");
	if(rv != 0)
	{
		perror("ERROR");
		return;
	}

	printf("cbuf->capacity: %i\n", cbuf->capacity);
	printf("cbuf->data: %s\n", cbuf->data);
	printf("strlen(cbuf->data): %i\n", strlen(cbuf->data));

	cbuf_del(cbuf);
	printf("OK: cbuf_wcscat()\n");
} /*test04()*/

void
test05(void)
{
} /*test05()*/

void
test06(void)
{
} /*test06()*/

void
test07(void)
{
} /*test07()*/

/* Test functions end -------------------------------------------------------*/


/* Add all test functions to this array -------------------------------------*/
void (*tests[])(void) = {
	test00,
	test01,
	test02,
	test03,
	test04,
	test05,
	test06,
	test07
};
/* Test array end -----------------------------------------------------------*/


/*
 * Do a test by the index given
 * i: index of test function
 */
static void
do_test(int i)
{
	printf("%02d ------------------------------------\n", i);
	freopen("/dev/stdout", "w", stdout);
	tests[i]();
	freopen("/dev/stdout", "w", stdout);
} /*do_test()*/


/*
 * Main computing
 * In case of no arguments given: performs all the tests
 * Alternatively performs only the tests given as arguments on the command line
 */
int
main(int argc, char **argv, char **envp)
{
	setlocale(LC_ALL, "");

	int i = 0;
	int j = 0;

	errno = 0;                               //initialize errno

	for(i=0; i < sizeof(tests)/sizeof(tests[0]); i++)
		if(argc == 1)
			do_test(i);
		else
			for(j=0; j < argc - 1; j++)
				if(atoi(argv[j+1]) == i)
					do_test(i);

     printf( "---------------------------------------\n");
     return 0;
} /*main()*/
