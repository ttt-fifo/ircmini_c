/*
 * Tests for ircmini.
 *
 * NOTE: you must have a functional IRC server listening on 127.0.0.1:6667 and
 *       you must login with irc client using nick 'recipient'
 */
#include <ircmini/ircmini.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>

//The nick of your test recipient
wchar_t *recipient = L"XXX";
//IRC server and port
char *server = "127.0.0.1";
char *port = "6667";

/* Create all test functions below ------------------------------------------*/

/*
 * Create and delete the ircmini object.
 */
void
test00(void)
{
	/*open irc socket*/
	Irc *irc = irc_new(server, port);
	if(irc == NULL)
	        perror("ERROR");
	else
		printf("Socket connected\n");
	        /*destruct socket*/
	        irc_del(irc);

} /*test00()*/

/*
 * Login to IRC server, then quit
 */
void
test01(void)
{
	Irc *irc = irc_new(server, port);
	if(irc == NULL)
	{
		perror("ERROR");
		return;
	}

	irc_raw(irc, L"USER ircmini ircmini ircmini :ircmini");
	irc_raw(irc, L"NICK ircmini");
	irc_raw(irc, L"QUIT :bye");

	irc_del(irc);
	printf("Logged in / Logged out\n");
} /*test01()*/

/*
 * Send PRIVMSG to the test recipient
 */
void
test02(void)
{
	Irc *irc = irc_new(server, port);
	if(irc == NULL)
	{
		perror("ERROR");
		return;
	}

	irc_raw(irc, L"USER ircmini ircmini ircmini :ircmini");
	irc_raw(irc, L"NICK ircmini");

	irc_privmsg(irc, recipient, L"Hello from ircmini");

	irc_raw(irc, L"QUIT :bye");

	irc_del(irc);
	printf("PRIVMSG sent\n");
} /*test02()*/

/*
 * Send unicode characters PRIVMSG
 */
void
test03(void)
{
	Irc *irc = irc_new(server, port);
	if(irc == NULL)
	{
		perror("ERROR");
		return;
	}

	irc_raw(irc, L"USER ircmini ircmini ircmini :ircmini");
	irc_raw(irc, L"NICK ircmini");

	irc_privmsg(irc, recipient, L"АЯ дв Ожбр");

	irc_raw(irc, L"QUIT :bye");

	irc_del(irc);
	printf("(unicode) PRIVMSG sent\n");
} /*test03()*/

/* Test functions end -------------------------------------------------------*/


/* Add all test functions to this array -------------------------------------*/
void (*tests[])(void) = {
	test00,
	test01,
	test02,
	test03
};
/* Test array end -----------------------------------------------------------*/


/*
 * Do a test by the index given
 * i: index of test function
 */
static void
do_test(int i)
{
	printf("%02d ------------------------------------\n", i);
	freopen("/dev/stdout", "w", stdout);
	tests[i]();
	freopen("/dev/stdout", "w", stdout);
} /*do_test()*/


/*
 * Main computing
 * In case of no arguments given: performs all the tests
 * Alternatively performs only the tests given as arguments on the command line
 */
int
main(int argc, char **argv, char **envp)
{
	setlocale(LC_ALL, "");

	int i = 0;
	int j = 0;

	errno = 0;                               //initialize errno

	for(i=0; i < sizeof(tests)/sizeof(tests[0]); i++)
		if(argc == 1)
			do_test(i);
		else
			for(j=0; j < argc - 1; j++)
				if(atoi(argv[j+1]) == i)
					do_test(i);

     printf( "---------------------------------------\n");
     return 0;
} /*main()*/
